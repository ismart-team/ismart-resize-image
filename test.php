<?php
include_once 'vendor/autoload.php';
include_once 'resize.php';

$img = new \Ismart\Resize('banner-promo.jpg', 'banners', array(
    1920 => 'xl',
    1200 => 'lg',
    900 => 'md',
    540 => 'sm',
    300 => 'xs'
));

$img->resize();
