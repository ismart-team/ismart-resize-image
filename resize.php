<?php
namespace Ismart;

class Resize
{
    private $image;
    private $dest_path;
    private $widthes;
    private $width_src;
    private $name;
    private $ext;

    public function __construct($src_path, $dest_path, $widthes)
    {
        $this->image = new \Eventviva\ImageResize($src_path);
        $this->dest_path = $dest_path;
        $this->widthes = $widthes;
        $this->width_src = $this->image->getSourceWidth();
        $this->name = explode('.', basename($src_path))[0];
        $this->ext = explode('.', basename($src_path))[1];
        $this->createDir();
    }

    private function generatePath($suffix)
    {
        return $this->dest_path . '/' . $this->name . '-' . $suffix . '.' . $this->ext;
    }

    private function createDir()
    {
        if (!file_exists($this->dest_path)) {
            mkdir($this->dest_path, 0777, true);
        }
    }

    public function resize()
    {
        foreach ($this->widthes as $width => $suffix) {
            $dest = $this->generatePath($suffix);
            if ($width >= $this->width_src) {
                $this->image->save($dest);
            } else {
                $this->image
                ->resizeToWidth($width)
                ->save($dest);
            }
        }
    }
}
